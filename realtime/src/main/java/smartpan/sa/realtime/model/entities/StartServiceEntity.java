package smartpan.sa.realtime.model.entities;

import java.io.Serializable;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 11/3/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class StartServiceEntity implements Serializable {

    private boolean startService;
    private Method method;

    public StartServiceEntity(boolean startService, Method method) {
        this.startService = startService;
        this.method = method;
    }

    public boolean isStartService() {
        return startService;
    }

    public void setStartService(boolean startService) {
        this.startService = startService;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public static enum Method {
        SOCKET, PUBNUB
    }
}
