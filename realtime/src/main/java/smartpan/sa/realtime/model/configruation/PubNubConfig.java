package smartpan.sa.realtime.model.configruation;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.pubnub.api.models.consumer.pubsub.PNSignalResult;
import com.pubnub.api.models.consumer.pubsub.message_actions.PNMessageActionResult;
import com.pubnub.api.models.consumer.pubsub.objects.PNMembershipResult;
import com.pubnub.api.models.consumer.pubsub.objects.PNSpaceResult;
import com.pubnub.api.models.consumer.pubsub.objects.PNUserResult;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 11/3/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class PubNubConfig extends SubscribeCallback {


    public static final String LOCATION_UPDATE_CHANNEL_ID = "location-update-channel";
    public static final String LOCATION_SERVICE_CHANNEL_ID = "location-service-channel";
    private static PubNubConfig pubNubConfig;
    private PubNub pubNub;
    private Listener listener;


    public static synchronized PubNubConfig getInstance(Context context) {
        if (pubNubConfig == null) {
            pubNubConfig = new PubNubConfig();
            pubNubConfig.initPubNub(context);
        }
        return pubNubConfig;
    }

    private void initPubNub(Context context) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-3243db28-fe29-11e9-8dd7-ca99873d233c");
        pnConfiguration.setPublishKey("pub-c-de3666f7-d039-40db-9209-3dffdaf73e50");


        pubNub = new PubNub(pnConfiguration);


    }


    public PubNub getPubNub() {
        return pubNub;
    }

    public void startListener(String channel, Listener listener) {
        this.listener = listener;
        try {

            pubNub.addListener(this);


            pubNub.subscribe()
                    .channels(Collections.singletonList(channel))
                    .execute();

        } catch (Exception e) {
            Log.e(PubNubConfig.class.getSimpleName(), "error  : ", e);
        }

    }


    public void publish(String channel, Object message) {
        pubNub.publish()
                .message(message)
                .channel(channel)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        if (!status.isError()) {
                            Log.e(PubNubConfig.class.getSimpleName(), "Message timetoken: " + result.getTimetoken());
                        } else {
                            Log.e(PubNubConfig.class.getSimpleName(), "error  : ", status.getErrorData().getThrowable());
                        }
                    }
                });

    }

    @Override
    public void status(@NotNull PubNub pubnub, @NotNull PNStatus pnStatus) {

    }

    @Override
    public void message(@NotNull PubNub pubnub, @NotNull PNMessageResult pnMessageResult) {
        listener.onNewMessage(pnMessageResult.getMessage().getAsJsonObject());
    }

    @Override
    public void presence(@NotNull PubNub pubnub, @NotNull PNPresenceEventResult pnPresenceEventResult) {

    }

    @Override
    public void signal(@NotNull PubNub pubnub, @NotNull PNSignalResult pnSignalResult) {

    }

    @Override
    public void user(@NotNull PubNub pubnub, @NotNull PNUserResult pnUserResult) {

    }

    @Override
    public void space(@NotNull PubNub pubnub, @NotNull PNSpaceResult pnSpaceResult) {

    }

    @Override
    public void membership(@NotNull PubNub pubnub, @NotNull PNMembershipResult pnMembershipResult) {

    }

    @Override
    public void messageAction(@NotNull PubNub pubnub, @NotNull PNMessageActionResult pnMessageActionResult) {

    }

    public void unbind() {
        pubNub.removeListener(this);

        pubNub.unsubscribeAll();
    }

    public interface Listener {
        void onNewMessage(JsonObject message);
    }
}
