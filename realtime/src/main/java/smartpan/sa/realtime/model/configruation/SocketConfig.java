package smartpan.sa.realtime.model.configruation;

import android.util.Log;

import com.google.gson.Gson;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import smartpan.sa.realtime.model.entities.StartServiceEntity;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 11/3/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */


/**
 * To  use socket.io you have to init server side that handle connections between clients
 * <p>
 * instruction for use
 * - install node js by navigate to `https://nodejs.org/en/download/` and choose your platform
 * - restart your pc
 * - start cmd on `SocketServer` folder which allocated in main root of project
 * - type `node index.js` this will start server side
 * - change BASE_URL with your local IP
 * - run app module in emulator and live_app module in another emulator
 */
public class SocketConfig {

    public static final String LOCATION_UPDATE_CHANNEL_ID = "location-update-channel";
    public static final String LOCATION_SERVICE_CHANNEL_ID = "location-service-channel";
    private static SocketConfig socketConfig;
    private String BASE_URL = "http://192.168.1.185";
    private Socket mSocket;
    private Listener listener;
    private Emitter.Listener onLocationReceive = args -> {
        Log.e("TAG_SOCKET", "onLocationReceive : " + args[0]);
        if (listener != null) listener.updateLocationUi(StartServiceEntity.Method.SOCKET, args[0]);
    };
    private Emitter.Listener onStartServiceReceive = args -> {
        Log.e("TAG_SOCKET", "onStartServiceReceive : " + args[0]);
        if (listener != null) listener.updateServerUi(StartServiceEntity.Method.SOCKET, args[0]);
    };

    private SocketConfig(Listener listener) {
        this.listener = listener;
    }

    public static synchronized SocketConfig getInstance(Listener listener) {
        if (socketConfig == null) {
            socketConfig = new SocketConfig(listener);
            socketConfig.initSocket();
        }
        return socketConfig;
    }

    private void initSocket() {
        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = true;

        try {
            mSocket = IO.socket(BASE_URL.concat(":3000/"));
        } catch (URISyntaxException e) {
            Log.e("TAG_SOCKET", "error : ", e);
        }


        initListener();


    }

    private void initListener() {
        mSocket
                .on(Socket.EVENT_CONNECT, args -> {

                    Log.e("TAG_SOCKET", "info : Socket.EVENT_CONNECT");
                })
                .on(LOCATION_UPDATE_CHANNEL_ID, onLocationReceive)
                .on(LOCATION_SERVICE_CHANNEL_ID, onStartServiceReceive)
                .on(Socket.EVENT_DISCONNECT, args -> {

                    Log.e("TAG_SOCKET", "info : Socket.EVENT_DISCONNECT");
                });

        mSocket.connect();
    }

    public void publish(String event, Object entity) {
        mSocket.emit(event, entity);
    }

    public Socket getSocket() {
        return mSocket;
    }


    private void off(String event, Emitter.Listener listener) {
        mSocket.off(event, listener);
    }

    public void unbind() {

        off(LOCATION_UPDATE_CHANNEL_ID, onLocationReceive);
        off(LOCATION_SERVICE_CHANNEL_ID, onStartServiceReceive);
        mSocket.disconnect();
    }

    public interface Listener {
        void updateLocationUi(StartServiceEntity.Method method, Object object);

        void updateServerUi(StartServiceEntity.Method method, Object object);
    }


}
