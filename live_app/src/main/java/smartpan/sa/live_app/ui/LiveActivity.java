package smartpan.sa.live_app.ui;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;


import smartpan.sa.live_app.R;
import smartpan.sa.live_app.databinding.ActivityLiveBinding;
import smartpan.sa.live_app.model.entities.LocationEntity;
import smartpan.sa.realtime.model.configruation.PubNubConfig;
import smartpan.sa.realtime.model.configruation.SocketConfig;
import smartpan.sa.realtime.model.entities.StartServiceEntity;

public class LiveActivity extends FragmentActivity implements SocketConfig.Listener {


    ActivityLiveBinding activityMainBinding;
    private StartServiceEntity startServiceEntity;


    private PubNubConfig pubNubConfig;
    private SocketConfig socketConfig = SocketConfig.getInstance(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_live);
        activityMainBinding.setLifecycleOwner(this);


        pubNubConfig = PubNubConfig.getInstance(this);

        startServiceEntity = new StartServiceEntity(true
                , activityMainBinding.toggleButton.isChecked()
                ? StartServiceEntity.Method.PUBNUB
                : StartServiceEntity.Method.SOCKET);


        activityMainBinding.toggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {

            /**
             *
             * isChecked --> false      this means  use  SOCKET.IO
             * isChecked --> true      this means  use  PUBNUB
             *
             * */
            startServiceEntity.setMethod(isChecked ? StartServiceEntity.Method.PUBNUB : StartServiceEntity.Method.SOCKET);


        });


        initPubNubListener();
    }

    private void initPubNubListener() {
        pubNubConfig.startListener(PubNubConfig.LOCATION_UPDATE_CHANNEL_ID, message -> {
            Log.e(LiveActivity.class.getSimpleName(), "message : " + message.toString());
            LocationEntity location = new Gson().fromJson(message, LocationEntity.class);
            handleLocationReceiver(StartServiceEntity.Method.PUBNUB, location);
        });
    }


    public void startServiceAction(View view) {

        startServiceEntity.setStartService(true);
        switch (startServiceEntity.getMethod()) {
            case PUBNUB:
                pubNubConfig.publish(PubNubConfig.LOCATION_SERVICE_CHANNEL_ID, startServiceEntity);
                break;

            case SOCKET:
                if (!socketConfig.getSocket().connected()) {
                    Snackbar.make(view, "Socket not Connected yet", Snackbar.LENGTH_LONG).show();
                    return;
                }


                socketConfig.publish(SocketConfig.LOCATION_SERVICE_CHANNEL_ID, new Gson().toJson(startServiceEntity));
                break;
        }


    }

    public void stopServiceAction(View view) {
        startServiceEntity.setStartService(false);

        switch (startServiceEntity.getMethod()) {
            case PUBNUB:
                pubNubConfig.publish(PubNubConfig.LOCATION_SERVICE_CHANNEL_ID, startServiceEntity);
                break;

            case SOCKET:
                if (!socketConfig.getSocket().connected()) {
                    Snackbar.make(view, "Socket not Connected yet", Snackbar.LENGTH_LONG).show();
                    return;
                }


                socketConfig.publish(SocketConfig.LOCATION_SERVICE_CHANNEL_ID, new Gson().toJson(startServiceEntity));
                break;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        socketConfig.unbind();
        pubNubConfig.unbind();


    }


    @Override
    public void updateLocationUi(StartServiceEntity.Method method, Object object) {
        handleLocationReceiver(method, new Gson().fromJson(object.toString(), LocationEntity.class));
    }

    private void handleLocationReceiver(StartServiceEntity.Method method, LocationEntity location) {
        if (location == null) return;
        runOnUiThread(() -> activityMainBinding.currentLocation.setText(method.name().concat("  :  ").concat(
                String.valueOf(location.getLatLng().latitude).concat(" , ".concat(String.valueOf(location.getLatLng().longitude))))));

    }

    @Override
    public void updateServerUi(StartServiceEntity.Method method, Object object) {

    }


}
