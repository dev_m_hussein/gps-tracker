package smartpan.sa.androidtest.ui.activities.main;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.maps.android.PolyUtil;

import org.apache.commons.collections4.CollectionUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import smartpan.sa.androidtest.databinding.ActivityMainBinding;
import smartpan.sa.androidtest.model.utils.GpsProviderHelper;
import smartpan.sa.androidtest.model.viewmodels.main.LocationViewModel;
import smartpan.sa.androidtest.ui.services.LocationService;
import smartpan.sa.realtime.model.entities.StartServiceEntity;


public class BaseMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {


    protected static final int LOCATION_PERMISSION_REQUEST_CODE = 15;
    private static final String TAG = BaseMapActivity.class.getSimpleName();
    protected LocationViewModel locationViewModel;
    protected GoogleMap googleMapView;
    protected ActivityMainBinding activityMainBinding;
    protected Location currentLocation;
    protected GpsProviderHelper gpsProviderHelper = new GpsProviderHelper() {
        @Override
        protected void onErrorMessage(String error) {
            super.onErrorMessage(error);
            Toast.makeText(BaseMapActivity.this, "error : " + error, Toast.LENGTH_LONG).show();
        }

        @Override
        protected Activity getActivity() {
            return BaseMapActivity.this;
        }

        @Override
        protected void onApiClientConnected() {
            super.onApiClientConnected();
            if (!GpsProviderHelper.needPermission(getActivity()))
                initializeMap();
        }

        @Override
        protected void onNeedPermissions() {
            super.onNeedPermissions();
            ActivityCompat.requestPermissions(BaseMapActivity.this
                    , new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION
                            , Manifest.permission.ACCESS_COARSE_LOCATION}
                    , LOCATION_PERMISSION_REQUEST_CODE
            );
        }
    };
    private FusedLocationProviderClient fusedLocationClient;
    private Polyline polyline;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationViewModel = new ViewModelProvider(this).get(LocationViewModel.class);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    protected void initMaps(Bundle savedInstanceState) {
        activityMainBinding.mapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        gpsProviderHelper.setupGoogleService();

    }

    private void initializeMap() {
        activityMainBinding.mapView.getMapAsync(this);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (activityMainBinding.mapView != null)
            activityMainBinding.mapView.onSaveInstanceState(outState);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMapView = googleMap;
        if (!GpsProviderHelper.needPermission(this))
            setUpMap();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        if (activityMainBinding.mapView != null)
            activityMainBinding.mapView.onStart();
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        if (activityMainBinding.mapView != null) activityMainBinding.mapView.onStop();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (activityMainBinding.mapView != null) activityMainBinding.mapView.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (activityMainBinding.mapView != null) activityMainBinding.mapView.onResume();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (activityMainBinding.mapView != null) activityMainBinding.mapView.onLowMemory();
    }


    /* setup Map in this fragment*/
    @SuppressLint("MissingPermission")
    private void setUpMap() {
        googleMapView.getUiSettings().setIndoorLevelPickerEnabled(true);
        googleMapView.getUiSettings().setAllGesturesEnabled(true);
        googleMapView.getUiSettings().setCompassEnabled(false);
        googleMapView.setMyLocationEnabled(true);
        googleMapView.getUiSettings().setMyLocationButtonEnabled(false);
        googleMapView.setOnMapLoadedCallback(this);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationViewModel.stopProviderListener();
    }

    protected void startLocationUpdateService(StartServiceEntity.Method method) {

        Snackbar.make(activityMainBinding.mapView, method.name() + "  start the service", Snackbar.LENGTH_LONG).show();
        if (GpsProviderHelper.needPermission(this)) return;
        locationViewModel.startProviderListener();
        initializeMap();
        if (!GpsProviderHelper.isGPSEnabled(this))
            gpsProviderHelper.enableGps();
        ActivityCompat.startForegroundService(this, new Intent(this, LocationService.class));
    }

    protected void stopLocationUpdateService(StartServiceEntity.Method method) {
        Snackbar.make(activityMainBinding.mapView, method.name() + "  stop the service", Snackbar.LENGTH_LONG).show();
        stopService(new Intent(this, LocationService.class));
    }


    protected void updateCameraToCurrentLocation() {
        /*
         * Zoom values
         *
         * max zoom out   0
         * max zoom in    19
         *
         * */
        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location == null) return;
            currentLocation = location;
            LatLng current = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            googleMapView.animateCamera(CameraUpdateFactory.newLatLngZoom(current, 17F), null);
        });

    }

    protected void addPolyline(Location location) {
        if (location == null) return;


        LatLng current = new LatLng(location.getLatitude(), location.getLongitude());

        addPolyline(current, Color.RED);

    }

    protected void addPolyline(LatLng latLng, @ColorInt int color) {
        if (latLng == null) return;


        if (polyline == null) {
            PolylineOptions options = new PolylineOptions().width(25).color(color).geodesic(true);
            options.add(latLng);
            polyline = googleMapView.addPolyline(options);
        } else {
            List<LatLng> points = polyline.getPoints();
            points.add(latLng);
            polyline.setPoints(points);
            polyline.setColor(color);
        }

    }

    protected void addOldPolyline(List<LatLng> latLngs, @ColorInt int color) {
        if (CollectionUtils.isEmpty(latLngs)) return;


        PolylineOptions options = new PolylineOptions().width(15).color(color).geodesic(true);
        options.addAll(latLngs);
        googleMapView.addPolyline(options);


    }


    protected void addOldPolyline(String polyline, @ColorInt int color) {
        if (TextUtils.isEmpty(polyline)) return;


        List<LatLng> points = PolyUtil.decode(polyline);
        PolylineOptions options = new PolylineOptions().width(20).color(color).geodesic(true);
        options.addAll(points);
        googleMapView.addPolyline(options);
        zoomToPound(points);


    }


    private void zoomToPound(List<LatLng> points) {
        if (googleMapView == null || CollectionUtils.isEmpty(points)) return;
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : points)
            boundsBuilder.include(latLngPoint);

        int routePadding = 100;
        LatLngBounds latLngBounds = boundsBuilder.build();

        googleMapView.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
    }

    protected void updateCameraLocation(Location location) {
        /*
         * Zoom values
         *
         * max zoom out   0
         * max zoom in    19
         *
         * */
        if (location == null) return;
        LatLng current = new LatLng(location.getLatitude(), location.getLongitude());
        googleMapView.animateCamera(CameraUpdateFactory.newLatLng(current), null);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("TAG_MAP_STATE", "onActivityResult");

        switch (requestCode) {
            case GpsProviderHelper.REQUEST_ENABLE_LOCATION_SERVICE:
                locationViewModel.setGpsEnabled(GpsProviderHelper.isGPSEnabled(this));
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE:
                if (GpsProviderHelper.needPermission(this)) {
                    // show dialog to explain permission purpose

                } else {
//                    startLocationUpdateService();
                }
                break;


        }
    }


    /**
     * Receive Event bus for Location object
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLocationReceive(Location location) {
        currentLocation = location;
        updateCameraLocation(location);
        addPolyline(location);


    }


    @Override
    public void onMapLoaded() {

    }
}
