package smartpan.sa.androidtest.ui.activities.main;

import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import smartpan.sa.androidtest.R;
import smartpan.sa.androidtest.model.entities.LocationEntity;
import smartpan.sa.androidtest.model.presenters.DBPresenter.DBModel;
import smartpan.sa.androidtest.model.presenters.DBPresenter.DBPresenter;
import smartpan.sa.androidtest.model.presenters.DBPresenter.DBView;
import smartpan.sa.androidtest.model.presenters.directionsPresenter.DirectionsModel;
import smartpan.sa.androidtest.model.presenters.directionsPresenter.DirectionsPresenter;
import smartpan.sa.androidtest.model.presenters.directionsPresenter.DirectionsView;
import smartpan.sa.androidtest.model.utils.GpsProviderHelper;
import smartpan.sa.realtime.model.configruation.PubNubConfig;
import smartpan.sa.realtime.model.configruation.SocketConfig;
import smartpan.sa.realtime.model.entities.StartServiceEntity;

public class MainActivity extends BaseMapActivity implements DBView, DirectionsView, SocketConfig.Listener {

    DBPresenter presenter = new DBModel(this);
    DirectionsPresenter directionsPresenter = new DirectionsModel(this);


    private StartServiceEntity startServiceEntity;


    private PubNubConfig pubNubConfig;
    private SocketConfig socketConfig = SocketConfig.getInstance(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setLifecycleOwner(this);
        activityMainBinding.setLocationViewModel(locationViewModel);


        pubNubConfig = PubNubConfig.getInstance(this);
        initMaps(savedInstanceState);

        observables();


//        if (!GpsProviderHelper.needPermission(this)) {
//            startLocationUpdateService();
//        }
        initPubNubListener();

    }

    private void initPubNubListener() {
        pubNubConfig.startListener(PubNubConfig.LOCATION_SERVICE_CHANNEL_ID, message -> {
            startServiceEntity = new Gson().fromJson(message.toString(), StartServiceEntity.class);
            handleServerAction(StartServiceEntity.Method.PUBNUB);

        });
    }


    private void observables() {
        locationViewModel.getGpsEnabledMutableLiveData().observe(this, gpsEnabled ->
//                activityMainBinding.fab.setImageResource(gpsEnabled ? R.drawable.ic_gps_on : R.drawable.ic_gps_off);
                activityMainBinding.fab.setActivated(gpsEnabled));
    }


    public void onGpsClick(View view) {
        if (GpsProviderHelper.isGPSEnabled(this)) {
            // gps enabled get current location and zoom map to it
            updateCameraToCurrentLocation();

            return;
        }
        // gps provider disables .. enable it
        gpsProviderHelper.enableGps();
    }

    @Override
    public void onLocationReceive(Location location) {
        super.onLocationReceive(location);
        if (location == null) return;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        LocationEntity locationEntity = new LocationEntity(latLng);
        presenter.addLocation(this, latLng);

        switch (startServiceEntity.getMethod()) {
            case PUBNUB:
                pubNubConfig.publish(PubNubConfig.LOCATION_UPDATE_CHANNEL_ID, locationEntity);
                break;

            case SOCKET:
                if (!socketConfig.getSocket().connected()) {
                    Snackbar.make(activityMainBinding.fab, "Socket not Connected yet", Snackbar.LENGTH_LONG).show();
                    break;
                }


                socketConfig.publish(SocketConfig.LOCATION_UPDATE_CHANNEL_ID, new Gson().toJson(locationEntity));
                break;
        }

    }

    @Override
    public void onMapLoaded() {
        super.onMapLoaded();
        presenter.getLocations(this);


        /**
         *
         * request directions
         * */


        LatLng from = new LatLng(30.057249, 31.345762);
        LatLng to = new LatLng(30.054390, 31.352026);

        directionsPresenter.getDirections(this, from, to);

    }

    @Override
    public void onLocationsResult(List<LatLng> locations) {
        if (CollectionUtils.isEmpty(locations)) return;

        addOldPolyline(locations, Color.BLUE);
    }

    @Override
    public void onRouteResult(String polyline) {

        addOldPolyline(polyline, Color.BLACK);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        socketConfig.unbind();


        pubNubConfig.unbind();
    }

    @Override
    public void updateLocationUi(StartServiceEntity.Method method, Object object) {

    }

    @Override
    public void updateServerUi(StartServiceEntity.Method method, Object object) {
        this.startServiceEntity = new Gson().fromJson(object.toString(), StartServiceEntity.class);
        handleServerAction(method);
    }

    private void handleServerAction(StartServiceEntity.Method method) {
        runOnUiThread(() -> {
            if (this.startServiceEntity.isStartService())
                startLocationUpdateService(method);
            else
                stopLocationUpdateService(method);
        });
    }
}
