package smartpan.sa.androidtest.repository.room;

import android.text.TextUtils;

import androidx.room.TypeConverter;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class RoomConverters {

    @TypeConverter
    public static LatLng latLngFromString(String latlng) {
        if (TextUtils.isEmpty(latlng)) return null;

        String[] splits = latlng.split(",");
        if (splits.length == 2) {
            return new LatLng(Double.parseDouble(splits[0]), Double.parseDouble(splits[1]));
        }
        return null;
    }

    @TypeConverter
    public static String latLngToString(LatLng latLng) {
        return latLng == null ? null : String.valueOf(latLng.latitude).concat(",").concat(String.valueOf(latLng.longitude));
    }

}
