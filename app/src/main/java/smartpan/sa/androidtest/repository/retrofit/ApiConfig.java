package smartpan.sa.androidtest.repository.retrofit;


import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiConfig {

    private final static int CACHE_SIZE_BYTES = 1024 * 1024 * 2;
    private static Retrofit retrofit;
    private static OkHttpClient client;

    /**
     * create single tone from retrofit so no recreate it with every request
     */
    public static Retrofit getRetrofit(Context context) {
        if (retrofit == null)
            retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .client(getClient(context))
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        return retrofit;

    }

    /**
     * this only when want to change base url for retrofit
     * so can recreate it again with another base url
     */
    public static void clear() {
        retrofit = null;
    }


    /**
     * this to define http request properties
     */
    private static OkHttpClient getClient(Context context) {
        if (client == null) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);
//            Interceptor interceptor = chain -> {
//                Request request = chain.request();
//                Response response = null;
//                try {
//                    response = chain.proceed(request);
//                    if (response.code() == 500) {
//
//
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    if (e instanceof ConnectException){
//                        EventBus.getDefault().post(new FinishApp(true , FinishApp.ActivityEnum.splash));
//                        context.startActivity(new Intent(context , SplashActivity.class));
//                    }
//                    return response;
//                }
//
//                return response;
//            };

            client = new OkHttpClient.Builder()
                    .cache(getCacheDir(context))
//                    .addInterceptor(interceptor)
                    .addInterceptor(loggingInterceptor)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();
        }

        return client;
    }


    /**
     * used to cache request response so it's better and faster
     * <p>
     * this used with E-TAG
     */

    private static Cache getCacheDir(Context context) {
        if (context == null) return null;
        return new Cache(context.getCacheDir(), CACHE_SIZE_BYTES);
    }


}
