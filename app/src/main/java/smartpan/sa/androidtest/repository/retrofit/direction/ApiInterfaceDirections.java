package smartpan.sa.androidtest.repository.retrofit.direction;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import smartpan.sa.androidtest.model.entities.direction.DirectionsResult;
import smartpan.sa.androidtest.repository.retrofit.ApiConstants;

interface ApiInterfaceDirections {


    @GET(ApiConstants.METHOD_DIRECTION)
    Observable<Response<DirectionsResult>> getDirections(
            @QueryMap Map<String, Object> query
    );

}
