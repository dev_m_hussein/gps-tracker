package smartpan.sa.androidtest.repository.room;

import android.content.Context;

import androidx.room.Room;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class RoomClient {

    private static RoomClient mInstance;
    private Context mCtx;
    //our app database object
    private AppDatabase appDatabase;

    private RoomClient(Context mCtx) {
        this.mCtx = mCtx;

        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mCtx, AppDatabase.class, AppDatabase.DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public static synchronized RoomClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new RoomClient(mCtx);
        }
        return mInstance;
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }

}
