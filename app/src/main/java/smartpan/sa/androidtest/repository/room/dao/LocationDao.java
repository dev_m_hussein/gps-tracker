package smartpan.sa.androidtest.repository.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import smartpan.sa.androidtest.model.entities.LocationEntity;


/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
@Dao
public interface LocationDao {

    @Query("SELECT * FROM locationEntity")
    List<LocationEntity> getAll();

    @Insert
    void insert(LocationEntity task);

    @Delete
    void delete(LocationEntity task);

    @Update
    void update(LocationEntity task);


}
