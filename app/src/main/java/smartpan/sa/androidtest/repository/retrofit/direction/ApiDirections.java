package smartpan.sa.androidtest.repository.retrofit.direction;

import android.content.Context;

import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import retrofit2.Retrofit;
import smartpan.sa.androidtest.model.entities.direction.DirectionsResult;
import smartpan.sa.androidtest.repository.retrofit.ApiConfig;

public class ApiDirections {
    private static ApiDirections apiServices;

    private static Retrofit retrofit;

    public static synchronized ApiDirections open(Context context) {
        if (retrofit == null)
            retrofit = ApiConfig.getRetrofit(context);
        if (apiServices == null) {
            apiServices = new ApiDirections();
        }
        return apiServices;
    }

    public static void clearRetrofit() {
        retrofit = null;
        ApiConfig.clear();
    }

    public Single<Response<DirectionsResult>> getDirectionsObservable(Map<String, Object> query) {

        ApiInterfaceDirections method = retrofit.create(ApiInterfaceDirections.class);

        return method
                .getDirections(query)
                .singleOrError()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


}
