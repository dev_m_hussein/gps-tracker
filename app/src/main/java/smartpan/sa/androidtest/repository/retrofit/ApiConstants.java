package smartpan.sa.androidtest.repository.retrofit;

public class ApiConstants {
    public static final String BASE_URL = "https://maps.googleapis.com/maps/api/";

    public static final String METHOD_DIRECTION = "directions/json";
}
