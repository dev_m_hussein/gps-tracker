package smartpan.sa.androidtest.repository.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import smartpan.sa.androidtest.model.entities.LocationEntity;
import smartpan.sa.androidtest.repository.room.dao.LocationDao;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */

@Database(entities = {LocationEntity.class}, version = 3)
@TypeConverters({RoomConverters.class})
public abstract class AppDatabase extends RoomDatabase {

    public static final String DB_NAME = "locations_gps";


    public abstract LocationDao locationDao();
}