package smartpan.sa.androidtest.model.entities.direction;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DirectionsResult {

    @SerializedName("routes")
    private List<RoutesItem> routes;

    @SerializedName("geocoded_waypoints")
    private List<GeocodedWaypointsItem> geocodedWaypoints;

    @SerializedName("status")
    private String status;

    public List<RoutesItem> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RoutesItem> routes) {
        this.routes = routes;
    }

    public List<GeocodedWaypointsItem> getGeocodedWaypoints() {
        return geocodedWaypoints;
    }

    public void setGeocodedWaypoints(List<GeocodedWaypointsItem> geocodedWaypoints) {
        this.geocodedWaypoints = geocodedWaypoints;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "DirectionsResult{" +
                        "routes = '" + routes + '\'' +
                        ",geocoded_waypoints = '" + geocodedWaypoints + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}