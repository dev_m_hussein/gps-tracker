package smartpan.sa.androidtest.model.presenters.directionsPresenter;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

public interface DirectionsPresenter {


    void getDirections(Context context, LatLng from, LatLng to);

}
