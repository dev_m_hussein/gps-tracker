package smartpan.sa.androidtest.model.viewmodels.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.location.LocationManagerCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import smartpan.sa.androidtest.model.utils.GpsProviderHelper;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class LocationViewModel extends AndroidViewModel {


    private final LocationManager locationManager;
    private MutableLiveData<Boolean> gpsEnabledMutableLiveData = new MutableLiveData<>(GpsProviderHelper.isGPSEnabled(getApplication().getBaseContext()));
    private MutableLiveData<String> gpsStateMutableLiveData = new MutableLiveData<>("");

    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
        }


        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            if (LocationManager.GPS_PROVIDER.equals(provider)) {
                setGpsEnabled(true);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            if (LocationManager.GPS_PROVIDER.equals(provider)) {
                setGpsEnabled(false);
            }
        }
    };


    public LocationViewModel(@NonNull Application application) {
        super(application);

        locationManager = (LocationManager) getApplication().getSystemService(Context.LOCATION_SERVICE);
    }


    public void setGpsEnabled(boolean enabled) {
        gpsEnabledMutableLiveData.setValue(enabled);
        gpsStateMutableLiveData.setValue(enabled ? "" : "Gps is disabled");
    }

    public MutableLiveData<Boolean> getGpsEnabledMutableLiveData() {
        return gpsEnabledMutableLiveData;
    }

    public MutableLiveData<String> getGpsStateMutableLiveData() {
        return gpsStateMutableLiveData;
    }

    @SuppressLint("MissingPermission")
    public void startProviderListener() {
        if (locationManager == null || GpsProviderHelper.needPermission(getApplication().getBaseContext()))
            return;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000000, 1000, listener);
    }

    public void stopProviderListener() {
        if (locationManager != null)
            locationManager.removeUpdates(listener);
    }

}
