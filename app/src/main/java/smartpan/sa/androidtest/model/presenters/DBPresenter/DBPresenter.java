package smartpan.sa.androidtest.model.presenters.DBPresenter;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public interface DBPresenter {


    void addLocation(Context context, LatLng latLng);

    void getLocations(Context context);
}
