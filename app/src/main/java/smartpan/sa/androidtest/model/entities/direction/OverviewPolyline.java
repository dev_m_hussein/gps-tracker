package smartpan.sa.androidtest.model.entities.direction;

import com.google.gson.annotations.SerializedName;

public class OverviewPolyline {

    @SerializedName("points")
    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return
                "OverviewPolyline{" +
                        "points = '" + points + '\'' +
                        "}";
    }
}