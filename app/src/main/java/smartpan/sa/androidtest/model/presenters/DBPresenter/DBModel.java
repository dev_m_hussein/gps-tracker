package smartpan.sa.androidtest.model.presenters.DBPresenter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.lifecycle.Observer;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import smartpan.sa.androidtest.model.entities.LocationEntity;
import smartpan.sa.androidtest.repository.room.RoomClient;

@SuppressLint("CheckResult")
public class DBModel implements DBPresenter {

    private DBView dbView;

    public DBModel(DBView dbView) {
        this.dbView = dbView;
    }

    @Override
    public void addLocation(Context context, LatLng latLng) {


        Observable.fromCallable(() -> {
            RoomClient.getInstance(context).getAppDatabase().locationDao().insert(new LocationEntity(latLng));
            return true;
        }).subscribeOn(Schedulers.newThread())
                .subscribe(aBoolean -> {
                    Log.e("TAG_DATABASE", "result : " + aBoolean);
                }, throwable -> {
                    Log.e("TAG_DATABASE", "error : ", throwable);
                });



    }


    @Override
    public void getLocations(Context context) {

        Observable.fromCallable(new Callable<List<LatLng>>() {
            @Override
            public List<LatLng> call() throws Exception {

                List<LocationEntity> locationEntities = RoomClient.getInstance(context).getAppDatabase().locationDao().getAll();
                List<LatLng> locations = new LinkedList<>();
                for (LocationEntity locationEntity : locationEntities) {
                    locations.add(locationEntity.getLatLng());
                }

                return locations;
            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(latLngs -> dbView.onLocationsResult(latLngs)
                        , throwable -> Log.e("TAG_DATABASE", "error : ", throwable)
                );


    }


}
