package smartpan.sa.androidtest.model.presenters.DBPresenter;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface DBView {


    void onLocationsResult(List<LatLng> locations);
}
