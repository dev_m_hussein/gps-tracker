package smartpan.sa.androidtest.model.presenters.directionsPresenter;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.collections4.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

import smartpan.sa.androidtest.R;
import smartpan.sa.androidtest.model.entities.direction.DirectionsResult;
import smartpan.sa.androidtest.repository.retrofit.direction.ApiDirections;

@SuppressLint("CheckResult")
public class DirectionsModel implements DirectionsPresenter {

    private DirectionsView directionsView;

    public DirectionsModel(DirectionsView dbView) {
        this.directionsView = dbView;
    }


    @Override
    public void getDirections(Context context, LatLng from, LatLng to) {

        Map<String, Object> query = new HashMap<>();

        query.put("origin", from.latitude + "," + from.longitude);
        query.put("destination", to.latitude + "," + to.longitude);
        query.put("key", context.getString(R.string.server_key));

        ApiDirections.open(context).getDirectionsObservable(query)
                .subscribe((response, throwable) -> {

                    if (response != null && response.isSuccessful()) {

                        DirectionsResult directionsResult = response.body();

                        if (directionsView != null && directionsResult != null
                                && !CollectionUtils.isEmpty(directionsResult.getRoutes()))
                            directionsView.onRouteResult(directionsResult.getRoutes().get(0).getOverviewPolyline().getPoints());
                    }
                });
    }
}
