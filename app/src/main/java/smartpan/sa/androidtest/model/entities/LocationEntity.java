package smartpan.sa.androidtest.model.entities;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 10/31/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */

@Entity(tableName = "locationEntity")
public class LocationEntity implements Serializable {



    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("locationId")
    private int locationId;


    @ColumnInfo(name = "latlng")
    @SerializedName("latLng")
    private LatLng latLng;


    public LocationEntity() {
    }

    public LocationEntity(LatLng latLng) {
        this.latLng = latLng;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

}
