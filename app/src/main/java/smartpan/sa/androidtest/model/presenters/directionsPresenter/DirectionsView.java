package smartpan.sa.androidtest.model.presenters.directionsPresenter;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public interface DirectionsView {


    void onRouteResult(String polyline);
}
